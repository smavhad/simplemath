
if [ ! -e "./DoAddition" ]
then
  echo "DoAddition executable does not exist"
  exit 1
fi

if [ ! -e "./DoFactorial" ]
then
  echo "DoFactorial executable does not exist"
  exit 1
fi

function PrintTestResult
{
  result=$1
  if [ $result -ne 0 ]
  then
    echo "Test Failed"
    exit $result
  else
    echo "Test Passed"
  fi
}

isSuccess=0

echo "Executing Test: DoAddition 7 8 9 10"
./DoAddition 7 8 9 10 | grep "34" 
isSuccess=$?
PrintTestResult $isSuccess


echo "Executing Test: DoAddition 2 3 45 3"
./DoAddition 2 3 45 3 | grep "53" 
isSuccess=$?
PrintTestResult $isSuccess

echo "Executing Test: DoFactorial 1"
./DoFactorial 1 | grep "1"
isSuccess=$?
PrintTestResult $isSuccess

echo "Executing Test: DoFactorial 5"
./DoFactorial 5 | grep "120"
isSuccess=$?
PrintTestResult $isSuccess

