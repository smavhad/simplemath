#include <iostream>
using namespace std;

int main (int argc, char* argv[])
{
  if (argc < 3)
  {
      cout << "You have forgot to type numbers." << endl;
      return 1;
  }
  int sum = 0;
  for (int i = 1; i < argc; i++)
      sum = sum + atoi(argv[i]);

  cout << "Result:" << sum << endl;

  return 0;
}

