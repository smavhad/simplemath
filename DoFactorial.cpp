#include <iostream>
using namespace std;

int main (int argc, char* argv[])
{
  if (argc != 2 )
  {
      cout << "You have entered incorrect arguments." << endl;
      return 1;
  }
  int fact = 1;
  int number = atoi(argv[1]);
  for (int i = 1; i <= number; i++)
      fact = fact * i;

  cout << "Result:" << fact << endl;

  return 0;
}

